# PILIS - MODULO 4 - PROYECTO FINAL 🔰

- Vilca Carlos Norberto Salvador

## 🎫 SISTEMA BACKEND DE RESERVA DE TICKETS

### Inntroduccion

Hola!👋, Este proyecto consiste en un sistema de reserva de tickets para eventos, el sistema cuenta con las entidades relacionadas, **User** para trabajar con datos Usuarios, **Event** para los Eventos y **Booking** para trabajar con las Reservas. Cada entidad cuenta con diferentes **endpoint** para poder manejar sus datos, y algunas requieren una autenticacion de usuario registrado.

El sistema valida, si hay limite de cupos en el evento y si se esta por registrar un booking, menor o igual al limite. Si el limite es cero, el evento no tiene limiete de cupo.

En este proyecto API Rest se trabaja con lo siguiente: **NodeJS**, **Express**, **Typescript**, **TypeORM**, **bcrypt**, **JWT**, **Swagger**

- Implementacion de Swagger [localhost:3000](http://localhost:3000/docs/) ✅ 
- implementacion de Relaciones✅

### Estructura
| User|Event|Bookings|
|-|-|-|
|id|id|id|
|email|nombre|precio|
|password|descripcion|fechaHora|
|active|lugar|lugar|
|createAt|fechaHora|gps|
|updatedAt|gps|id_event|
||precio|id_user|
||limite||
||tipoEvento||

- User
    - POST Signup
    - POST Signin
    - GET (auth)
    - POST (auth)
    - PUT (auth)
    - DELETE (auth)
- Event
    - GET
    - POST (auth)
    - PUT (auth)
    - DELETE (auth)
- Booking
    - GET (auth)
    - POST (auth)
    - PUT (auth)
    - DELETE (auth)

## 📦 INSTALACION

Para iniciar este proyecto se debe contar con lo siguiente:
- Visual Studio Code
- Docker
- Postman
- Dbeaver (opcional)

###  Pasos 
1. Instalar las dependencias 
```bash
    npm install
```
2. Iniciar la BD: *ubicarse en la carpeta **.\docker-compose\mysql***
```bash
    cd .\docker-compose\mysql\
```
```bash
    docker-compose up -d
```
3. ejecutar la aplicacion
```bash
    cd ../../
```
```bash
    npm run dev
```
## 📌ENLACES UTILES
- [Visual Studio Code](https://code.visualstudio.com)
- [Docker](https://docs.docker.com/get-docker/)
- [Postman](https://www.postman.com/)
- [DBeaver](https://dbeaver.io/)