import { Request, Response } from "express";
import { Event } from "../entity/Event";
import { Booking } from "../entity/Booking";

interface EventBody {
    nombre: string;
    descripcion: string;
    lugar: string;
    fechaHora: Date;
    gps: string;
    limite: number;
    tipoEvento: string;
}
/**
 * devuelve una lista con todos los eventos 
 * @param req 
 * @param res 
 * @returns 
 */
export const getEvents = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {
        const events = await Event.find({
            relations: {
                bookings: true
            },
            select:{
                bookings: {
                    id: true
                }
            }
        });
        console.log('events: ---->'), events;
        return res.json(events);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * devuelve un Evento segun el id mandado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const getEvent = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const event = await Event.findOne({
            where: { id: parseInt(id) },
            relations: ['bookings']
        });

        if (!event) return res.status(404).json({ message: "Event not found" });
        return res.json(event);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
}

/**
 * metodo que crea un evento y lo guarda en la tabla
 * @param req 
 * @param res 
 * @returns 
 */
export const createEvent = async (req: Request, res: Response) => {
    const { nombre, descripcion, lugar, fechaHora, gps, limite,precio, tipoEvento} = req.body;
    const arrayBookings: Booking[] = [];


    const event = new Event();
    event.nombre = nombre;
    event.descripcion = descripcion;
    event.lugar = lugar;
    event.fechaHora = fechaHora;
    event.gps = gps;
    event.precio = precio;
    event.limite = limite;
    event.tipoEvento = tipoEvento;

    event.bookings = arrayBookings;

    await event.save();
    return res.json(event);
}
/**
 * actualiza los datos de un evento dependiendo del id pasado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const updateEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const event = await Event.findOneBy({ id: parseInt(id) });
        if (!event) return res.status(404).json({ message: "Not event found" });
        await Event.update({ id: parseInt(id) }, req.body);
        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
}
/**
 * elimina un evento dependiendo del id pasado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const deleteEvent = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await Event.delete({ id: parseInt(id) });
        if (result.affected === 0)
            return res.status(404).json({ message: "Event not found" });

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
}