import { Request, Response } from "express";
import { Booking } from "../entity/Booking";
import { Event } from "../entity/Event";
import { User } from "../entity/User";

interface BookingBody {
    precio: number;
    fechaHora: Date;
    lugar: string;
    gps: string;
    id_event: number;
    id_user: number;
}

/**
 * devuelve una lista con todas las reservas
 * @param req 
 * @param res 
 * @returns 
 */
export const getBookings = async (req: Request, res: Response) => {
    console.log('entrando...');
    try {
        const bookings = await Booking.find({
            relations: {
                event: true,
                user: true,
            },
            select: {
                event: {
                    id: true,
                    nombre: true,
                },
                user: {
                    id: true,
                    email: true,
                }
            }
        });
        console.log('bookings: ---->'), bookings;
        return res.json(bookings);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message })
        }
    }
};

/**
 * devuelve una reserva segun el id mandado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const getBooking = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const booking = await Booking.findOne({
            where: { id: parseInt(id) },
            relations: {
                event: true,
                user: true,
            },
            select: {
                event: {
                    id: true,
                    nombre: true,
                    descripcion: true,
                    tipoEvento: true,
                    limite: true,
                },
                user: {
                    id: true,
                    email: true,
                    active: true,
                }
            }
        });
        if (!booking) return res.status(404).json({ message: "Booking not found" })
        return res.json(booking);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};

/**
 * Crea una nueva reserva y la guarda en la tabla
 * @param req 
 * @param res 
 * @returns 
 */
export const createBooking = async (req: Request, res: Response) => {
    const { id_event, id_user } = req.body;
    const bookedEvent = <Event>await Event.findOneBy({ id: parseInt(id_event) });
    const bookingUser = <User>await User.findOneBy({ id: parseInt(id_user) });
    const booking = new Booking();
    booking.precio = bookedEvent.precio;
    booking.fechaHora = bookedEvent.fechaHora;
    booking.lugar = bookedEvent.lugar;
    booking.gps = bookedEvent.gps;
    booking.event = bookedEvent;
    booking.user = bookingUser;
    await booking.save();
    return res.json(booking);
}
/**
 * actualiza los datos de una reserva dependiendo del id pasado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const updateBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
    const { id_event, id_user } = req.body;
    try {
        const booking = <Booking>await Booking.findOneBy({ id: parseInt(id) });
        const bookedEvent = <Event>await Event.findOneBy({ id: parseInt(id_event) });
        const bookingUser = <User>await User.findOneBy({ id: parseInt(id_user) });
        booking.precio = bookedEvent.precio;
        booking.fechaHora = bookedEvent.fechaHora;
        booking.lugar = bookedEvent.lugar;
        booking.gps = bookedEvent.gps;
        booking.event = bookedEvent;
        booking.user = bookingUser;
        if (!booking) return res.status(404).json({ message: "Not booking found" })
        await Booking.update({ id: parseInt(id) }, booking);
        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
}

/**
 * elimina una reserva dependiendo del id pasado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const deleteBooking = async (req: Request, res: Response) => {
    const { id } = req.params;
    try {
        const result = await Booking.delete({ id: parseInt(id) });
        if (result.affected === 0)
            return res.status(404).json({ message: "Booking not found" });

        return res.sendStatus(204);
    } catch (error) {
        if (error instanceof Error) {
            return res.status(500).json({ message: error.message });
        }
    }
};