import { Request, Response } from "express";
import { User } from "../entity/User";

// -------- Agregar para jwt
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Booking } from "../entity/Booking";
const jwtSecret = 'somesecrettoken';
const jwtRefreshTokenSecret = 'somesecrettokenrefresh';
let refreshTokens: (string | undefined)[] = [];

/**
 * genera los token y tokenrefresh
 * @param user 
 * @returns 
 */
const createToken = (user: User) => {
  const token = jwt.sign({ id: user.id, email: user.email }, jwtSecret, { expiresIn: '180s' });
  const refreshToken = jwt.sign({ email: user.email }, jwtRefreshTokenSecret, { expiresIn: '90d' });

  refreshTokens.push(refreshToken);
  return {
    token,
    refreshToken
  }
}

interface UserBody {
  firstname: string;
  lastname: string;
}
/**
 * obtener una lista de todos los usuarios y sus reservas 
 * @param req 
 * @param res 
 * @returns 
 */
export const getUsers = async (req: Request, res: Response) => {
  console.log('entrando...');
  try {
    const users = await User.find({
      relations: {
        bookings: true
      },
      select:{
          bookings: {
              id: true
          }
      }
    });
    console.log('users: --->'), users;
    return res.json(users);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

/**
 * obtener un usuario dependiendo del id pasado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const getUser = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const user = await User.findOne({
      where: { id: parseInt(id) },
      relations: ['bookings']
    });

    if (!user) return res.status(404).json({ message: "User not found" });

    return res.json(user);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

/**
 * registra un nuevo usuarios con los datos pasados por el body
 * @param req 
 * @param res 
 * @returns 
 */
export const createUser = async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const userFound = await User.findOneBy({ email: req.body.email });
  if (userFound) {
    return res.status(400).json({ msg: "The User already Exists" });
  }

  const arrayBookings: Booking[] = [];
  const user = new User();
  user.email = email;
  user.password = await createHash(password);
  user.bookings = arrayBookings;

  await user.save();
  return res.json(user);
};

/**
 * modifica los datos de un usuario dependiendo de id pasado por parametro
 * @param req 
 * @param res 
 * @returns 
 */
export const updateUser = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { email, password } = req.body

  try {
    const user = await User.findOneBy({ id: parseInt(id) });
    if (!user) return res.status(404).json({ message: "Not user found" });

    const userUpdate = new User();
    userUpdate.email = email
    userUpdate.password = await createHash(password);
    await User.update({ id: parseInt(id) }, userUpdate);

    return res.sendStatus(204);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

/**
 * metodo para eliminar un usuario segun su id
 * @param req 
 * @param res 
 * @returns 
 */
export const deleteUser = async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const result = await User.delete({ id: parseInt(id) });

    if (result.affected === 0)
      return res.status(404).json({ message: "User not found" });

    return res.sendStatus(204);
  } catch (error) {
    if (error instanceof Error) {
      return res.status(500).json({ message: error.message });
    }
  }
};

// agregar para JWT
/**
 * crear una cuenta de usuario, registra un usuario nuevo 
 * usando los datos del body
 * el password es encriptado
 * @param req 
 * @param res 
 * @returns 
 */
export const signUp = async (req: Request, res: Response): Promise<Response> => {
  if (!req.body.email || !req.body.password) {
    return res
      .status(400)
      .json({ msg: "Please. Send your email and password" });
  }

  const user = await User.findOneBy({ email: req.body.email });
  if (user) {
    return res.status(400).json({ msg: "The User already Exists" });
  }
  const newUser = new User();
  newUser.email = req.body.email;
  newUser.password = await createHash(req.body.password);
  await newUser.save();
  return res.status(201).json(newUser);
};

/**
 * encripta un password pasado por parametro
 * usando bcrypt
 * @param password 
 * @returns 
 */
const createHash = async (password: string): Promise<string> => {
  const saltRounds = 10;
  return await bcrypt.hash(password, saltRounds);
};

/**
 * metodo login de usuario
 * verifica que el usuario este en la base de datos 
 * compara los passwords
 * y genera el token y refresh token
 * @param req 
 * @param res 
 * @returns 
 */
export const signIn = async (req: Request, res: Response): Promise<Response> => {
  if (!req.body.email || !req.body.password) {
    return res
      .status(400)
      .json({ msg: "Please. Send your email and password" });
  }

  const user = await User.findOneBy({ email: req.body.email });
  if (!user) {
    return res.status(400).json({ msg: "The User does not exists" });
  }

  const isMatch = await comparePassword(user, req.body.password);
  if (isMatch) {
    return res.status(200).json({ credentials: createToken(user) });
  }

  return res.status(400).json({
    msg: "The email or password are incorrect"
  });
};
/**
 * compara dos passpord y retonra un boolean 
 * usando bcrypt
 * @param user 
 * @param password 
 * @returns 
 */
const comparePassword = async (user: User, password: string): Promise<Boolean> => {
  return await bcrypt.compare(password, user.password);
};

/**
 * se usa para verificar si el token esta activo
 * @param req 
 * @param res 
 * @returns 
 */
export const protectedEndpoint = async (req: Request, res: Response): Promise<Response> => {

  return res.status(200).json({ msg: 'ok' });
}

/**
 * crea un nuevo access token usando el refresh token
 * @param req 
 * @param res 
 * @returns 
 */
export const refresh = async (req: Request, res: Response): Promise<any> => {
  const refreshToken = req.body.refresh;

  // If token is not provided, send error message
  if (!refreshToken) {
    res.status(401).json({
      errors: [
        {
          msg: "Token not found",
        },
      ],
    });
  }
  console.log(refreshTokens);
  // If token does not exist, send error message
  if (!refreshTokens.includes(refreshToken)) {
    res.status(403).json({
      errors: [
        {
          msg: "Invalid refresh token",
        },
      ],
    });
  }
  try {
    const user = jwt.verify(refreshToken, jwtRefreshTokenSecret);
    const { email } = <any>user;

    const userFound = <User>await User.findOneBy({ email: email });
    if (!userFound) {
      return res.status(400).json({ msg: "The User does not exists" });
    }

    const accessToken = jwt.sign({ id: userFound.id, email: userFound.email }, jwtSecret, { expiresIn: '120s' });

    res.json({ accessToken });
  } catch (error) {
    res.status(403).json({
      errors: [
        {
          msg: "Invalid token",
        },
      ],
    });
  }
};