import { NextFunction, Request, Response } from "express";
import { Event } from "../entity/Event";

/**
 * valida si hay limite de cupos en el evento
 * si es 0 no hay limite de cupos
 * @param req 
 * @param res 
 * @param next 
 */
const middlewareVerificarLimite = async (req: Request, res: Response, next: NextFunction) => {
    const event = <Event>await Event.findOne({
        where: { id: parseInt(req.body.id_event) },
        relations: ['bookings']
    });

    if (event.limite == 0) {
        console.log('el limite es 0, puede reservar')
        next()
    } else {
        if (event.bookings.length < event.limite) {
            console.log('hay lugar, puede reservar')
            next()
        } else {
            console.log('ya no hay lugar para reservar')
            res.json({
                message: 'no hay lugar para reservar en este evento',
                nombreEvento: event.nombre,
                cantidadReservados: event.bookings.length,
                limite: event.limite
            })
        }
    }

}

module.exports = {
    middlewareVerificarLimite
}