import { Router } from "express";
import { getBookings, getBooking, createBooking, updateBooking, deleteBooking } from "../controllers/booking.controller";
import passport from "passport";

const router = Router();
const { middlewareVerificarLimite } = require('../middlewares/middlewares')

router.get("/bookings", passport.authenticate('jwt', { session: false }), getBookings);
router.get("/bookings/:id", passport.authenticate('jwt', { session: false }), getBooking);
router.post("/bookings", passport.authenticate('jwt', { session: false }),middlewareVerificarLimite, createBooking);
router.put("/bookings/:id", passport.authenticate('jwt', { session: false }), updateBooking);
router.delete("/bookings/:id", passport.authenticate('jwt', { session: false }), deleteBooking);

export default router;
